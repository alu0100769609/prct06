require "biblio/bibliography.rb"

describe Bibliography do

   before :each do
      @bib1 = Bibliography.new("Autor1","Titulo1","Fecha1","ISBN1")
#      @bib1 = Bibliography.new(2,1,1,2)
#      @bib1.autor = ["Autor1", "Autor2"]
#      @bib1.titulo = "Titulo1"
#      @bib1.fecha = "Fecha1"
#      @bib1.isbn = ["ISBN1", "ISBN2"]
   end

   describe "Existe un libro" do

      it "El nombre del autor es correcto" do
         @bib1.autor.should eq("Autor1")
 #        @bib1.autor.should eq("Autor1","Autor2")
      end

      it "El título del libro es correcto" do
         @bib1.titulo.should eq("Titulo1")
      end

      it "La fecha del libro es correcta" do
         @bib1.fecha.should eq("Fecha1")
      end

      it "ISBN correcto" do
         @bib1.isbn.should eq("ISBN1")
#         @bib1.isbn.should eq("ISBN1","ISBN2")
      end
   end

end
